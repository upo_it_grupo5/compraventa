/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import DAO.DAOCategoria;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;
import modelo.Categoria;

/**
 *
 * @author Adolfo
 */
public class listadoCategoria extends ActionSupport{
    private DAOCategoria dao = new DAOCategoria();
    private List<Categoria> listaCategoria = new ArrayList<Categoria>();
    private int idCategoria;
    private String nombre;
    private String descripcion;
    private int activo;

    public DAOCategoria getDao() {
        return dao;
    }

    public void setDao(DAOCategoria dao) {
        this.dao = dao;
    }

    public List<Categoria> getListaCategoria() {
        return listaCategoria;
    }

    public void setListaCategoria(List<Categoria> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    
    
    public listadoCategoria() {
    }

    public String execute() throws Exception {
        listaCategoria = dao.consultaCategorias();
        
        return SUCCESS;
    }
    
    public String eliminarCategoria() {
        listaCategoria = dao.eliminarCategoria(idCategoria);
        
        return SUCCESS;
    }
    
    
    
    public String anadirCategoria() {
        Categoria c = new Categoria(nombre, descripcion, activo);
        
        listaCategoria = dao.anadirCategoria(c);
        
        return SUCCESS;
    }
}
