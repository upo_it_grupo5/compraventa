/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.Anunciodestacado;
import modelo.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author dominieliaspereirademattos
 */
public class DAOAnuncio {
    Session ses = null;
    
    public List<Anunciodestacado> consultarAnuncio() {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        Query q = ses.createQuery("from anunciodestacado");
        List<Anunciodestacado> l = (List<Anunciodestacado>) q.list();

        tx.commit();

        return l;
    }
    
    public List<Anunciodestacado> eliminarAnuncio(int idAnuncio) {
        Anunciodestacado p = consultarAnuncioPorId(idAnuncio);
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.delete(p);

        Query q = ses.createQuery("from anunciodestacado");
        List<Anunciodestacado> l = (List<Anunciodestacado>) q.list();

        tx.commit();

        return l;

    }
    
    public Anunciodestacado consultarAnuncioPorId(int idAnuncio) {
        Anunciodestacado p = null;

        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        Query q = ses.createQuery("from anunciodestacado where idAnuncio = '" + idAnuncio + "'");
        p = (Anunciodestacado) q.uniqueResult();

        tx.commit();

        return p;
    }
    
    public List<Anunciodestacado> insertarAnuncio(Anunciodestacado p) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.save(p);

        Query q = ses.createQuery("from anunciodestacado");
        List<Anunciodestacado> l = (List<Anunciodestacado>) q.list();

        tx.commit();

        return l;
    }
    
    public List<Anunciodestacado> modificarAnuncio(Anunciodestacado p) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.update(p);

        Query q = ses.createQuery("from anunciodestacado");
        List<Anunciodestacado> l = (List<Anunciodestacado>) q.list();

        tx.commit();

        return l;
    }
}
