/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author er_jo
 */
public class DAOUsuario {

    Session ses = null;
    public DAOUsuario() {
       this.ses = HibernateUtil.getSessionFactory().getCurrentSession();
    }

    public List consultarTodosUsuarios() {
        org.hibernate.Transaction tx = ses.beginTransaction();
        Query q = ses.createQuery("from usuario");
        List l = (List) q.list();
        tx.commit();
        return l;
    }

    public Usuario consultarUsuario(int idUsuario) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        Query q = ses.createQuery("from usuario where idUsuario =" + idUsuario);
        Usuario e = (Usuario) q.list();
        tx.commit();
        return e;
    }

    public List<Usuario> addUsuario(Usuario e) {

        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.save(e);
        Query q = ses.createQuery("from usuario");
        List<Usuario> u = (List<Usuario>) q.list();
        tx.commit();
        return u;

    }

    public List<Usuario> eliminarUsuario(Usuario e) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.delete(e);
        Query q = ses.createQuery("from usuario");
        List<Usuario> u = (List<Usuario>) q.list();
        tx.commit();
        return u;

    }

    public List<Usuario> modificarUsuario(Usuario e) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.update(e);
        Query q = ses.createQuery("from usuario");
        List<Usuario> u = (List<Usuario>) q.list();
        tx.commit();
        return u;
    }
}
