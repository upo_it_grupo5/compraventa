/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Categoria;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Adolfo
 */
public class DAOCategoria {
    Session ses = null;
    
    public List<Categoria> consultaCategorias() {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM categoria WHERE activo=1");
        List<Categoria> l = (List<Categoria>) q.list();
        
        tx.commit();
        
        return l;
    }
    
    public List<Categoria> consultaCategoriaPorId(int id) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM categoria WHERE idCategoria="+id);
        List<Categoria> l = (List<Categoria>) q.list();
        
        tx.commit();
        
        return l;
    }
    
    public List<Categoria> anadirCategoria(Categoria c) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.save(c);

        Query q = ses.createQuery("from Categoria");
        List<Categoria> l = (List<Categoria>) q.list();

        tx.commit();

        return l;
    }
    
    public List<Categoria> eliminarCategoria(int idCategoria) {
        Categoria c = (Categoria) consultaCategoriaPorId(idCategoria);
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.delete(c);

        Query q = ses.createQuery("from Categoria");
        List<Categoria> l = (List<Categoria>) q.list();

        tx.commit();

        return l;

    }
}
