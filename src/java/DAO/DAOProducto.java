/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Producto;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Adolfo
 */
public class DAOProducto {
    Session ses = null;
    
    public List<Producto> consultaProductos() {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM Producto WHERE activo=1");
        List<Producto> l = (List<Producto>) q.list();
        
        tx.commit();
        
        return l;
    }
    
    public List<Producto> consultaProductosPorId(int id) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM Producto WHERE idProducto="+id);
        List<Producto> l = (List<Producto>) q.list();
        
        tx.commit();
        return l;
    }
    
    public List<Producto> anadirProducto(Producto p) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.save(p);

        Query q = ses.createQuery("from producto");
        List<Producto> l = (List<Producto>) q.list();

        tx.commit();

        return l;
    }
    
    public List<Producto> eliminarProducto(int id) {
        Producto p = (Producto) consultaProductosPorId(id);
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.delete(p);

        Query q = ses.createQuery("from Producto");
        List<Producto> l = (List<Producto>) q.list();

        tx.commit();

        return l;

    }
    
}
