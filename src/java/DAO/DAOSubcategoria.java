/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Subcategoria;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Adolfo
 */
public class DAOSubcategoria {
    Session ses = null;
    
    public List<Subcategoria> consultaSubcategoria() {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM Subcategoria WHERE activo=1");
        List<Subcategoria> l = (List<Subcategoria>) q.list();
        
        tx.commit();
        
        return l;
    }
    
    public List<Subcategoria> consultaSubcategoriaPorId(int id) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();
        
        Query q = ses.createQuery("FROM subcategoria WHERE idSubcategoria="+id);
        List<Subcategoria> l = (List<Subcategoria>) q.list();
        
        tx.commit();
        return l;
    }
    public List<Subcategoria> anadirSubcategoria(Subcategoria s) {
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.save(s);

        Query q = ses.createQuery("from subcategoria");
        List<Subcategoria> l = (List<Subcategoria>) q.list();

        tx.commit();

        return l;
    }
    
    public List<Subcategoria> eliminarSubcategoria(int id) {
        Subcategoria s = (Subcategoria) consultaSubcategoriaPorId(id);
        ses = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = ses.beginTransaction();

        ses.delete(s);

        Query q = ses.createQuery("from Subcategoria");
        List<Subcategoria> l = (List<Subcategoria>) q.list();

        tx.commit();

        return l;

    }
}
