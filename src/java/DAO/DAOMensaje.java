/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import modelo.HibernateUtil;
import modelo.Mensaje;
import modelo.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author er_jo
 */
public class DAOMensaje {

    Session ses = null;
    public DAOMensaje() {
        this.ses = HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
    public List consultarTodosLosMensajes() {
        org.hibernate.Transaction tx = ses.beginTransaction();
        Query q = ses.createQuery("from mensaje");
        List l = (List) q.list();
        tx.commit();
        return l;
    }

    public Mensaje consultarMensaje(int idUsuarioEmisor) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        Query q = ses.createQuery("from mensaje where idUsuarioEmisor =" + idUsuarioEmisor);
        Mensaje e = (Mensaje) q.list();
        tx.commit();
        return e;
    }

    public List<Mensaje> addMensaje(Mensaje e) {

        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.save(e);
        Query q = ses.createQuery("from mensaje");
        List<Mensaje> u = (List<Mensaje>) q.list();
        tx.commit();
        return u;

    }

    public List<Mensaje> eliminarMensaje(Mensaje e) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.delete(e);
        Query q = ses.createQuery("from mensaje");
        List<Mensaje> u = (List<Mensaje>) q.list();
        tx.commit();
        return u;

    }

    public List<Mensaje> modificarMensaje(Mensaje e) {
        org.hibernate.Transaction tx = ses.beginTransaction();
        ses.update(e);
        Query q = ses.createQuery("from mensaje");
        List<Mensaje> u = (List<Mensaje>) q.list();
        tx.commit();
        return u;
    }
    
    
}
