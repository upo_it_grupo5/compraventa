<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <s:include value="cabecera.html" />
    <body>
        <h1>Insertar producto</h1>
        <s:form action="anadirProducto" validate="true">
            <s:textfield key="titulo" label="Titulo"></s:textfield>
            <s:textfield key="descripcion" label="Descripción"></s:textfield>
            <s:textfield key="precio" label="Precio"></s:textfield>
            <s:textfield key="provincia" label="Provincia"></s:textfield>
            <s:submit value="Insertar"></s:submit>
        </s:form>
    </body>
</html>
