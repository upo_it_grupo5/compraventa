<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <s:include value="cabecera.html" />
    <body>
        <h1>Insertar categoría</h1>
        <s:form action="anadirCategoria" validate="true">
            <s:textfield key="nombre" label="Nombre"></s:textfield>
            <s:textfield key="descripcion" label="Descripción"></s:textfield>
            <s:submit value="Insertar"></s:submit>
        </s:form>
    </body>
</html>
